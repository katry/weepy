from .conftest import Server
from re import compile
from requests import get
from json import loads


def test_get():
	def app_wrapper():
		from weepy import ASGI, Route
		app = ASGI(content_type="application/json")

		@Route("/", content_type="text/plain")
		async def root_get(req, resp):
			return "TEXT_RESPONSE_FROM_ROOT"

		@Route("/json")
		async def json_get(req, resp):
			return {"reponse": "json-response"}

		@Route("/query-params")
		async def query_param_get(req, resp):
			return {"query_params": req.query_params}

		@Route(compile(r"/text.+"), content_type="text/plain")
		async def regex(req, resp):
			return "text jak hovado"

		@Route(compile(r"/param-(.*)"))
		async def rege_param(req, resp, param):
			return {"param": param}

		return app

	server = Server(app_wrapper)

	response = get("http://localhost:3456")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == "TEXT_RESPONSE_FROM_ROOT"

	response = get("http://localhost:3456/json/")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:16] == "application/json"
	assert loads(response.text) == {"reponse": "json-response"}

	response = get("http://localhost:3456/query-params?jezevec=pes")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:16] == "application/json"
	assert loads(response.text) == {"query_params": {"jezevec": "pes"}}

	response = get("http://localhost:3456/text-")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == "text jak hovado"

	response = get("http://localhost:3456/text")
	assert response.status_code == 404

	response = get("http://localhost:3456/param-jezevec")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:16] == "application/json"
	assert loads(response.text) == {"param": "jezevec"}

	server.stop()
