from .conftest import Server
from re import compile
from requests import post, put
from json import loads


def test_post():
	def app_wrapper():
		from weepy import ASGI, Route
		app = ASGI(content_type="application/json")

		@Route("/post", methods=["POST"])
		async def post(req, resp):
			return req.data

		@Route("/put", methods=["PUT"])
		async def put(req, resp):
			return req.data

		return app

	server = Server(app_wrapper)

	response = post("http://localhost:3456/post", json={"input": "test-post"})
	assert response.status_code == 200
	assert response.headers["Content-Type"][:16] == "application/json"
	assert loads(response.text) == {"input": "test-post"}

	response = put("http://localhost:3456/put", json={"input": "test-put"})
	assert response.status_code == 200
	assert response.headers["Content-Type"][:16] == "application/json"
	assert loads(response.text) == {"input": "test-put"}

	server.stop()
